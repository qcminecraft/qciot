/*
 * Copyright (c) 2015 - 2018. ,QCTech
 * All rights reserved.
 */
//对于IE版本的判断
function IEVersion() {
    var userAgent = navigator.userAgent;
    var isIE = userAgent.indexOf("compatible") > -1 && userAgent.indexOf("MSIE") > -1; //IE<11
    var isEdge = userAgent.indexOf("Edge") > -1 && !isIE; //IE-Edge
    var isIE11 = userAgent.indexOf('Trident') > -1 && userAgent.indexOf("rv:11.0") > -1;
    if(isIE) {
        var reIE = new RegExp("MSIE (\\d+\\.\\d+);");
        reIE.test(userAgent);
        var fIEVersion = parseFloat(RegExp["$1"]);
        if(fIEVersion == 7) {
            return 7;
        } else if(fIEVersion == 8) {
            return 8;
        } else if(fIEVersion == 9) {
            return 9;
        } else if(fIEVersion == 10) {
            return 10;
        } else {
            return 6;//IE<=7
        }
    } else if(isEdge) {
        return 'edge';//edge
    } else if(isIE11) {
        return 11; //IE11
    }else{
        return -1;//Not IE
    }
}
//链接处理
function url(url) {
    if(url.indexOf('http') != -1){
        //站外链接
        window.open(url)

    }else if(url == '#'){

    } else {
        //站内链接
        $('#loading').fadeIn();
        setTimeout(function () {
            window.location.href = url
        }, 500)
    }
}
//音效播放
function playSound(file_name)
{
    var borswer = window.navigator.userAgent.toLowerCase();
    if ( borswer.indexOf( "ie" ) >= 0 )
    {
        //IE内核浏览器
        var strEmbed = '<embed name="embedPlay" src="' + file_name +
            '.wav" autostart="true" hidden="true" loop="false"></embed>';
        if ( $( "body" ).find( "embed" ).length <= 0 )
            $( "body" ).append( strEmbed );
        var embed = document.embedPlay;

        //浏览器不支持 audion，则使用 embed 播放
        embed.volume = 100;
        //embed.play();这个不需要
    } else {
        //非IE内核浏览器
        var strAudio = "<audio id='audioPlay' src='/static/sounds/" +file_name +
            ".wav' hidden='true'>";
        if ( $( "body" ).find( "audio" ).length <= 0 )
            $( "body" ).append( strAudio );
        var audio = document.getElementById( "audioPlay" );

        //浏览器支持 audion
        audio.play();
    }
}
//请求主服务器
$.ajax({url:"https://open.qcminecraft.com/html/qciot/",complete:function(result){
        if(result.statusText != 'OK'){
            playSound('Foreground');
            mdui.snackbar({
                message: '发生错误：无法连接到服务器',
            });
        }else {
            if(Cookies.get('qciot_na') != 'true') {
                playSound('Notify_System_Generic');
                mdui.snackbar({
                    buttonText: '不再提示',
                    message: result.responseJSON.word,
                    onButtonClick: function () {
                        Cookies.set('qciot_na', 'true', {expires: 31});
                    },
                });
            }
        }
}});
$(document).ready(function () {
    if(IEVersion() == -1){
        //console.log('恭喜，您没有在用IE')
    }
})