<?php
/**
*  ___   ____ ___ ___ _____
* / _ \ / ___|_ _/ _ \_   _|
 *| | | | |    | | | | || |
 *| |_| | |___ | | |_| || |
 * \__\_\\____|___\___/ |_|
 **/
namespace app\common\controller;

class Index
{
    public function index()
    {
        $hello = new self();
        return $hello->hello('World');
    }

    public function hello($name = 'Think php')
    {
        return 'Hello ' . $name;
    }
}