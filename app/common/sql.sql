-- phpMyAdmin SQL Dump
-- version 4.0.10.19
-- https://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2018-06-10 21:35:00
-- 服务器版本: 5.6.35-log
-- PHP 版本: 5.4.45

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `qciot`
--

-- --------------------------------------------------------

--
-- 表的结构 `qc_nav`
--

CREATE TABLE IF NOT EXISTS `qc_nav` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '导航ID',
  `pid` int(11) NOT NULL COMMENT '上级导航id',
  `type` text NOT NULL,
  `title` text NOT NULL,
  `icon` text NOT NULL,
  `color` text NOT NULL,
  `url` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='导航' AUTO_INCREMENT=7 ;

--
-- 转存表中的数据 `qc_nav`
--

INSERT INTO `qc_nav` (`id`, `pid`, `type`, `title`, `icon`, `color`, `url`) VALUES
(1, 0, 'bottom', '首页', 'home', '', '/index/index/index'),
(2, 0, 'bottom', 'Issue', 'feedback', '', 'https://gitee.com/qcminecraft/qciot/issues/new?issue%5Bassignee_id%5D=0&issue%5Bmilestone_id%5D=0'),
(3, 0, 'nav_1', '开始', 'near_me', 'blue', '#'),
(4, 3, 'nav_2', '首页', 'home', 'red', '/');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
