<?php
/**
 *  ___   ____ ___ ___ _____
 * / _ \ / ___|_ _/ _ \_   _|
 *| | | | |    | | | | || |
 *| |_| | |___ | | |_| || |
 * \__\_\\____|___\___/ |_|
 * Copyright (c) 2015 - 2018. ,QCTech
 * All rights reserved.
 */

namespace app\index\controller;

use app\common\model\Nav;
use think\Controller as tC;
use think\Request as tR;

class Index extends tC
{
    /**
     * @return mixed
     */
    public function index()
    {
        $tR = new tR();
        if($tR->get('theme/s') != '') {
            cookie('theme', $tR->get('theme/s'), 365.242199 * 24 * 3600);
            $this->redirect($tR->module().'/');
        }
        $nav_bottom = Nav::all(function ($query){
            $query->where("type", "eq", "bottom");
        });
        $nav_1 = Nav::all(function ($query){
            $query->where("type", "eq", "nav_1");
        });
        $nav_2 = Nav::all(function ($query){
            $query->where("type", "eq", "nav_2");
        });


        $this->assign([
            'theme' => cookie('theme'),
            'name'  => '首页',
            'nav_bottom' => $nav_bottom,
            'nav_1' => $nav_1,
            'nav_2' => $nav_2,
        ]);
        
        return $this->fetch('common@index');


    }
    public function offline()
    {
        $tR = new tR();
        if($tR->get('theme/s') != '') {
            cookie('theme', $tR->get('theme/s'), 365.242199 * 24 * 3600);
            $this->redirect($tR->module().'/');
        }
        $nav_bottom = Nav::all(function ($query){
            $query->where("type", "eq", "bottom");
        });
        $nav_1 = Nav::all(function ($query){
            $query->where("type", "eq", "nav_1");
        });
        $nav_2 = Nav::all(function ($query){
            $query->where("type", "eq", "nav_2");
        });


        $this->assign([
            'theme' => cookie('theme'),
            'name'  => '离线',
            'nav_bottom' => $nav_bottom,
            'nav_1' => $nav_1,
            'nav_2' => $nav_2,
        ]);

        return $this->fetch('common@offline');


    }
}