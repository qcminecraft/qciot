# QCIOT - 青草物联
   ___   ____ ___ ___ _____
  / _ \ / ___|_ _/ _ \_   _|
 | | | | |    | | | | || |
 | |_| | |___ | | |_| || |
  \__\_\\____|___\___/ |_|
## 鸣谢
1. [Thinkphp](https://gitee.com/liu21st/thinkphp5)
2. [MDUI](https://gitee.com/zdhxiong/mdui)
3. [JQuery](https://jquery.com/)

## 环境
#### php
建议使用php 5.6，其他版本未知
#### 拓展
- php-pdo
- php-gd
- php-curl
- php-mbstring

## 启动方法
1. 修改配置文件：数据库等
2. 导入sql.sql
3. `$ ./think run`   （若使用其他Web软件可忽略此条）

## Demo
[https://lab.qcminecraft.com/](https://lab.qcminecraft.com/) - 最新版  **可能不稳定** 每小时从git仓库更新 